package tech.mastertech.itau.produto.models;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Pedido {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  @ManyToMany
  @NotEmpty
  private List<Produto> produtos;
  @ManyToOne
  @NotNull
  private Fornecedor fornecedor;
  private LocalDate data;
  
  public int getId() {
    return id;
  }
  public void setId(int id) {
    this.id = id;
  }
  public List<Produto> getProdutos() {
    return produtos;
  }
  public void setProdutos(List<Produto> produtos) {
    this.produtos = produtos;
  }
  public Fornecedor getFornecedor() {
    return fornecedor;
  }
  public void setFornecedor(Fornecedor fornecedor) {
    this.fornecedor = fornecedor;
  }
  public LocalDate getData() {
    return data;
  }
  public void setData(LocalDate data) {
    this.data = data;
  }
}
