package tech.mastertech.itau.produto.controllers;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.transaction.Transactional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.repositories.CategoriaRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@Sql(scripts = "classpath:dados.sql")
public class CategoriaControllerIT {
  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private CategoriaRepository categoriaRepository;
  
  private ObjectMapper mapper = new ObjectMapper();
//  private Categoria categoria;
  
//  @Before
//  public void preparar() {
//    categoria = new Categoria();
//    categoria.setNome("Álcool");
//    
//    categoriaRepository.save(categoria);
//  }
  
  @Test
  public void deveBuscarTodasAsCategorias() throws Exception {
//    List<Categoria> categorias = Lists.list(categoria);
    
    mockMvc.perform(get("/categorias"))
            .andExpect(status().isOk())
            .andExpect(content().string("[{\"id\":1,\"nome\":\"Álcool\"}]"));
  }

  @WithMockUser
  @Test
  public void deveSalvarUmaCategoria() throws Exception {
    Categoria categoria = new Categoria();
    categoria.setNome("UmaCategoria");
    String categoriaJson = mapper.writeValueAsString(categoria);
    
    mockMvc.perform(
        post("/auth/categoria")
        .content(categoriaJson)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
       )
      .andExpect(status().isOk())
      .andExpect(content().string(containsString(categoria.getNome())));
  }
}
